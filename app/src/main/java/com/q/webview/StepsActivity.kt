package com.q.webview

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.work.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.*
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.request.DataUpdateRequest
import com.google.android.gms.tasks.Task
import com.q.webview.services.AlarmService
import kotlinx.android.synthetic.main.activity_steps.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt


class StepsActivity : AppCompatActivity() {
    val TAG = "BasicHistoryApi"
    enum class FitActionRequestCode {
        INSERT_AND_READ_DATA,
        UPDATE_AND_READ_DATA,
        DELETE_DATA
    }

    private val dateFormat = DateFormat.getDateInstance()
    private val fitnessOptions: FitnessOptions by lazy {
        FitnessOptions.builder()
                .addDataType(DataType.TYPE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_CALORIES_EXPENDED, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.TYPE_WEIGHT, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.TYPE_WEIGHT, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
                .build()
    }

//    var myService: ForegroundService? = null
    var isBound = false
    private var fileReadWrite: InternalFileReadWrite? = null
    private lateinit var workManager: WorkManager
    private lateinit var oneTimeReq: OneTimeWorkRequest
    private lateinit var periodicReq: PeriodicWorkRequest

    /*private val myConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            val binder = service as ForegroundService.MyLocalBinder
            myService = binder.getService()
            isBound = true

//            getBackgroundNotification(applicationContext, myService).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        }

        override fun onServiceDisconnected(name: ComponentName) {

            isBound = false
        }
    }*/


    private fun startService() {
        /*val serviceClass = ForegroundService::class.java
        val serviceIntent = Intent(applicationContext, serviceClass)

        // If the service is not running then start it
        if (!isServiceRunning(serviceClass)) {
            // Start the service
            startService(serviceIntent)
            bindService(serviceIntent, myConnection, Context.BIND_AUTO_CREATE)
        } else {
            Log.i("tag", "Service already running.")
            bindService(serviceIntent, myConnection, Context.BIND_AUTO_CREATE)
        }*/

        val intent = Intent(application, AlarmService::class.java)
        intent.putExtra("REQUEST_CODE", 1)
        ContextCompat.startForegroundService(this@StepsActivity, intent)
    }


    // Custom method to determine whether a service is running
    private fun isServiceRunning(serviceClass: Class<*>): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        // Loop through the running services
        for (service in activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                // If the service is running then return true
                return true
            }
        }
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_steps)

        supportFragmentManager.beginTransaction()
                .add(android.R.id.content, FitDataFragment()).commit()

//        fitSignIn(FitActionRequestCode.INSERT_AND_READ_DATA)

        workManager = WorkManager.getInstance(this)

        fileReadWrite = InternalFileReadWrite(applicationContext)

        btnStart.setOnClickListener {
//            startService()
//            startPeriodicRequest()
//            readHistoryData()
        }

        btnPastWeight.setOnClickListener {
            readPast()
        }

        btnPastCal.setOnClickListener {
            readPastCal()
        }
        btnStop.setOnClickListener {
            val intent = Intent(application, AlarmService::class.java)
            stopService(intent)
//            workManager.cancelWorkById(periodicReq.id)
        }

        btnReadData.setOnClickListener {
            log_text.setText(fileReadWrite!!.readFile())
        }
    }

    private fun startPeriodicRequest() {
        val constraints = Constraints.Builder()
                .build()

        val data = Data.Builder()
                .putString("channel_id", "periodic_work")
                .putString("title", "Periodic Request")
                .putString("message", "This notification is from Periodic Request")
                .build()

        periodicReq = PeriodicWorkRequest.Builder(MyWorker::class.java, 1, TimeUnit.HOURS)
                .setConstraints(constraints)
                .setInputData(data)
                .build()

        workManager.enqueueUniquePeriodicWork("start_periodic_work", ExistingPeriodicWorkPolicy.REPLACE, periodicReq)
    }

    /**
     * Gets a Google account for use in creating the Fitness client. This is achieved by either
     * using the last signed-in account, or if necessary, prompting the user to sign in.
     * `getAccountForExtension` is recommended over `getLastSignedInAccount` as the latter can
     * return `null` if there has been no sign in before.
     */
    private fun getGoogleAccount() = GoogleSignIn.getAccountForExtension(this, fitnessOptions)

    private fun oAuthPermissionsApproved() = GoogleSignIn.hasPermissions(getGoogleAccount(), fitnessOptions)

    /**
     * Checks that the user is signed in, and if so, executes the specified function. If the user is
     * not signed in, initiates the sign in flow, specifying the post-sign in function to execute.
     *
     * @param requestCode The request code corresponding to the action to perform after sign in.
     */
    public fun fitSignIn(requestCode: FitActionRequestCode) {
        if (oAuthPermissionsApproved()) {
//            performActionForRequestCode(requestCode)
            readHistoryData()
        } else {
            requestCode.let {
                GoogleSignIn.requestPermissions(
                        this,
                        requestCode.ordinal,
                        getGoogleAccount(), fitnessOptions)
            }
        }
    }
    /*private fun performActionForRequestCode(requestCode: FitActionRequestCode) = when (requestCode) {
//        FitActionRequestCode.INSERT_AND_READ_DATA -> insertAndReadData()
        FitActionRequestCode.UPDATE_AND_READ_DATA -> updateAndReadData()
//        FitActionRequestCode.DELETE_DATA -> deleteData()
    }*/
    /**
     * Handles the callback from the OAuth sign in flow, executing the post sign in function
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                val postSignInAction = FitActionRequestCode.values()[requestCode]
                postSignInAction.let {
                    readHistoryData()
//                    performActionForRequestCode(postSignInAction)
                }
            }
            else -> oAuthErrorMsg(requestCode, resultCode)
        }
    }

    private fun oAuthErrorMsg(requestCode: Int, resultCode: Int) {
        val message = """
            There was an error signing into Fit. Check the troubleshooting section of the README
            for potential issues.
            Request code was: $requestCode
            Result code was: $resultCode
        """.trimIndent()
        Log.e(TAG, message)
    }


    // update

    /**
     * Updates and reads data by chaining [Task] from [.updateData] and [ ][.readHistoryData].
     */
//    private fun updateAndReadData() = updateData().continueWithTask { readHistoryData() }

    /**
     * Creates a [DataSet],then makes a [DataUpdateRequest] to update step data. Then
     * invokes the History API with the HistoryClient object and update request.
     */
    private fun updateData(): Task<Void> {
        // Create a new dataset and update request.
        val dataSet = updateFitnessData()
        val startTime = dataSet.dataPoints[0].getStartTime(TimeUnit.MILLISECONDS)
        val endTime = dataSet.dataPoints[0].getEndTime(TimeUnit.MILLISECONDS)
        // [START update_data_request]
        Log.i(TAG, "Updating the dataset in the History API.")

        val request = DataUpdateRequest.Builder()
                .setDataSet(dataSet)
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
                .build()

        // Invoke the History API to update data.
        return Fitness.getHistoryClient(this, getGoogleAccount())
                .updateData(request)
                .addOnSuccessListener {
                    android.util.Log.e(TAG, "Data update was successful.")
                }
                .addOnFailureListener { e ->
                    android.util.Log.e(TAG, "There was a problem updating the dataset.", e)
                }
    }

    /** Creates and returns a {@link DataSet} of step count data to update. */
    private fun updateFitnessData(): DataSet {
        Log.i(TAG, "Creating a new data update request.")


        val dfEnd: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss a")
        val dateEnd = dfEnd.format(Calendar.getInstance().time)
        val dateEnd1: Date = dfEnd.parse(dateEnd)!!
        val endTimeNew = dateEnd1.time
        Log.i(TAG, "Range End1: ${dateFormat.format(endTimeNew)}")

        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd 00:00:01 a")
        val date = df.format(Calendar.getInstance().time)
        val date1: Date = df.parse(date)!!
        val startDateNew = date1.time
        Log.i(TAG, "Range Start1: ${date}")

        // Create a data source
        val dataSource = DataSource.Builder()
                .setAppPackageName(this)
                .setDataType(DataType.TYPE_CALORIES_EXPENDED)
                .setStreamName("$TAG - step count")
                .setType(DataSource.TYPE_RAW)
                .build()

        // Create a data set
        val stepCountDelta = 1000f
        // For each data point, specify a start time, end time, and the data value -- in this case,
        // the number of new steps.
        return DataSet.builder(dataSource)
                .add(DataPoint.builder(dataSource)
                        .setField(Field.FIELD_CALORIES, stepCountDelta)
                        .setTimeInterval(startDateNew, endTimeNew, TimeUnit.MILLISECONDS)
                        .build()
                ).build()
        // [END build_update_data_request]
    }

    /**
     * Asynchronous task to read the history data. When the task succeeds, it will print out the
     * data.
     */
    private fun readHistoryData() {
        val cal = Calendar.getInstance()
        val calstart = Calendar.getInstance()
//        calstart.add(Calendar.WEEK_OF_MONTH,-1)
//        val now = Date()
//        cal.time = now
        cal.add(Calendar.WEEK_OF_MONTH, -1)
        val startTime = calstart.timeInMillis
        val endTime = cal.timeInMillis

        val dateFormat: DateFormat = DateFormat.getDateInstance()
        android.util.Log.e(TAG, "Range Start: " + dateFormat.format(startTime))
        android.util.Log.e(TAG, "Range End: " + dateFormat.format(endTime))
        val dataReadRequest = DataReadRequest.Builder()
                .read(DataType.TYPE_WEIGHT)
                .setTimeRange(endTime, startTime, TimeUnit.MILLISECONDS)
                .setLimit(1)
                .build()

        Fitness.getHistoryClient(this, GoogleSignIn.getAccountForExtension(this, fitnessOptions))
                .readData(dataReadRequest)
                .addOnSuccessListener { dataReadResponse ->
                    android.util.Log.e("System out", "dataReadResponse:${dataReadResponse.getDataSet(DataType.TYPE_WEIGHT).dataPoints[0].getValue(Field.FIELD_WEIGHT)}")
                    val weight = if (dataReadRequest == null) 0 else dataReadResponse.getDataSet(DataType.TYPE_WEIGHT).dataPoints[0].getValue(Field.FIELD_WEIGHT)
                    txtWeight.text = "Current Weight:- ${weight}"
                }
                .addOnFailureListener { e: Exception ->
                    android.util.Log.e("System out", "FAIL")
                }

        Fitness.getHistoryClient(this, GoogleSignIn.getAccountForExtension(this, fitnessOptions))
                .readDailyTotal(DataType.TYPE_CALORIES_EXPENDED)
                .addOnSuccessListener { result: DataSet ->
//                    val totalSteps = if (result.isEmpty) 0 else result.dataPoints[0].getValue(Field.FIELD_WEIGHT)
                    android.util.Log.e("System out", "total Step:$result")
                    val cal: Float = if (result.isEmpty) 0f else result.dataPoints[0].getValue(Field.FIELD_CALORIES).asFloat()
                    txtCal.text = "Cal:- ${cal.roundToInt()}"
                }
                .addOnFailureListener { e: Exception ->
                    android.util.Log.e("FragmentActivity.TAG", "There was a problem getting steps: " +
                            e.localizedMessage)
                }

        /*Fitness.getHistoryClient(this, GoogleSignIn.getAccountForExtension(this, fitnessOptions))
                .readDailyTotal(DataType.TYPE_WEIGHT)
                .addOnSuccessListener { result: DataSet ->
//                    val totalSteps = if (result.isEmpty) 0 else result.dataPoints[0].getValue(Field.FIELD_WEIGHT)
                    Log.e("System out", "total Step:$result")
                }
                .addOnFailureListener { e: Exception ->
                    Log.i("FragmentActivity.TAG", "There was a problem getting steps: " +
                            e.localizedMessage)
                }*/

    }

    private fun readPast() {
        val cal = Calendar.getInstance()
        val calstart = Calendar.getInstance()
//        calstart.add(Calendar.WEEK_OF_MONTH,-1)
//        val now = Date()
//        cal.time = now
        cal.add(Calendar.MONTH, -1)
        val startTime = calstart.timeInMillis
        val endTime = cal.timeInMillis

        val dateFormat: DateFormat = DateFormat.getDateInstance()
        android.util.Log.e(TAG, "Range Start: " + dateFormat.format(startTime))
        android.util.Log.e(TAG, "Range End: " + dateFormat.format(endTime))
        val dataReadRequest = DataReadRequest.Builder()
                .read(DataType.TYPE_WEIGHT)
                .setTimeRange(endTime, startTime, TimeUnit.MILLISECONDS)
                .setLimit(30)
                .build()

        Fitness.getHistoryClient(this, GoogleSignIn.getAccountForExtension(this, fitnessOptions))
                .readData(dataReadRequest)
                .addOnSuccessListener { dataReadResponse ->
                    android.util.Log.e("System out", "dataReadResponse:${dataReadResponse.getDataSet(DataType.TYPE_WEIGHT).dataPoints[0].getValue(Field.FIELD_WEIGHT)}")
                    val weight = dataReadResponse.getDataSet(DataType.TYPE_WEIGHT).dataPoints[0].getValue(Field.FIELD_WEIGHT)
//                    txtWeight.text = "Weight:- ${weight}"
                    txtWeight.append(":::Past Weight:- ${weight}")
                }
                .addOnFailureListener { e: Exception ->
                    android.util.Log.e("System out", "FAIL")
                }

    }

    private fun readPastCal() {
        val cal = Calendar.getInstance()//
        val calstart = Calendar.getInstance()
//        calstart.add(Calendar.WEEK_OF_MONTH,-1)
//        val now = Date()
//        cal.time = now
        cal.add(Calendar.MONTH, -1)
        val startTime = calstart.timeInMillis
        val endTime = cal.timeInMillis

        val dataReadRequest = DataReadRequest.Builder()
                .read(DataType.TYPE_CALORIES_EXPENDED)
                .setTimeRange(endTime, startTime, TimeUnit.MILLISECONDS)
                .setLimit(30)
                .build()

        Fitness.getHistoryClient(this, GoogleSignIn.getAccountForExtension(this, fitnessOptions))
                .readData(dataReadRequest)
                .addOnSuccessListener { dataReadResponse ->
                    android.util.Log.e("System out", "dataReadResponse:${dataReadResponse.getDataSet(DataType.TYPE_CALORIES_EXPENDED).dataPoints[0].getValue(Field.FIELD_CALORIES)}")
                    val cal: Float = dataReadResponse.getDataSet(DataType.TYPE_CALORIES_EXPENDED).dataPoints[0].getValue(Field.FIELD_CALORIES).asFloat()
//                    txtWeight.text = "Weight:- ${weight}"
                    txtCal.append(":::Past Cal:- ${cal.roundToInt()}")
                }
                .addOnFailureListener { e: Exception ->
                    android.util.Log.e("FragmentActivity.TAG", "There was a problem getting steps: " +
                            e.localizedMessage)
                }
    }

    private fun queryFitnessData(): DataReadRequest {
        // [START build_read_data_request]
        // Setting a start and end date using a range of 1 week before this moment.
//        val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
//        val now = Date()
//        calendar.time = now
//        val endTime = calendar.timeInMillis
//        calendar.add(Calendar.DAY_OF_MONTH, 1)
//        val startTime = calendar.timeInMillis

//        Log.i(TAG, "Range Start: ${dateFormat.format(startTime)}")
        val dfEnd: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss a")
        val dateEnd = dfEnd.format(Calendar.getInstance().time)
        val dateEnd1: Date = dfEnd.parse(dateEnd)!!
        val endTimeNew = dateEnd1.time
        Log.i(TAG, "Range End1: ${dateFormat.format(endTimeNew)}")

        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd 00:00:01 a")
        val date = df.format(Calendar.getInstance().time)
        val date1: Date = df.parse(date)!!
        val startDateNew = date1.time
        Log.i(TAG, "Range Start1: ${date}")

        return DataReadRequest.Builder()
                // The data request can specify multiple data types to return, effectively
                // combining multiple data queries into one call.
                // In this example, it's very unlikely that the request is for several hundred
                // datapoints each consisting of a few steps and a timestamp.  The more likely
                // scenario is wanting to see how many steps were walked per day, for 7 days.
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                // Analogous to a "Group By" in SQL, defines how data should be aggregated.
                // bucketByTime allows for a time span, whereas bucketBySession would allow
                // bucketing by "sessions", which would need to be defined in code.
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startDateNew, endTimeNew, TimeUnit.MILLISECONDS)
                .build()
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("Service status", "Running")
                return true
            }
        }
        Log.i("Service status", "Not running")
        return false
    }

}