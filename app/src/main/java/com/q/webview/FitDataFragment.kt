package com.q.webview

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataSet
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.DataReadRequest
import com.q.webview.services.AlarmService
import kotlinx.android.synthetic.main.activity_steps.*
import java.text.DateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt


class FitDataFragment : Fragment() {
    val TAG = "FitDataFragment"

    enum class FitActionRequestCode {
        INSERT_AND_READ_DATA,
        UPDATE_AND_READ_DATA,
        DELETE_DATA
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fitSignIn(StepsActivity.FitActionRequestCode.INSERT_AND_READ_DATA)

        btnStart.setOnClickListener {
            val intent = Intent(context!!, AlarmService::class.java)
            intent.putExtra("REQUEST_CODE", 1)
            ContextCompat.startForegroundService(context!!, intent)
        }

        btnPastWeight.setOnClickListener {
            readPast()
        }

        btnPastCal.setOnClickListener {
            readPastCal()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fit_data, container, false)
    }

    public fun fitSignIn(requestCode: StepsActivity.FitActionRequestCode) {
        if (oAuthPermissionsApproved()) {
//            performActionForRequestCode(requestCode)
            readHistoryData()
        } else {
            requestCode.let {
                GoogleSignIn.requestPermissions(
                        this,
                        requestCode.ordinal,
                        getGoogleAccount(), fitnessOptions)
            }
        }
    }

    private fun oAuthPermissionsApproved() = GoogleSignIn.hasPermissions(getGoogleAccount(), fitnessOptions)

    private fun getGoogleAccount() = GoogleSignIn.getAccountForExtension(activity!!, fitnessOptions)

    private val fitnessOptions: FitnessOptions by lazy {
        FitnessOptions.builder()
                .addDataType(DataType.TYPE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_CALORIES_EXPENDED, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.TYPE_WEIGHT, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.TYPE_WEIGHT, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
                .build()
    }

    private fun readHistoryData() {
        val cal = Calendar.getInstance()
        val calstart = Calendar.getInstance()
//        calstart.add(Calendar.WEEK_OF_MONTH,-1)
//        val now = Date()
//        cal.time = now
        cal.add(Calendar.WEEK_OF_MONTH, -1)
        val startTime = calstart.timeInMillis
        val endTime = cal.timeInMillis

        val dateFormat: DateFormat = DateFormat.getDateInstance()
        android.util.Log.e(TAG, "Range Start: " + dateFormat.format(startTime))
        android.util.Log.e(TAG, "Range End: " + dateFormat.format(endTime))
        val dataReadRequest = DataReadRequest.Builder()
                .read(DataType.TYPE_WEIGHT)
                .setTimeRange(endTime, startTime, TimeUnit.MILLISECONDS)
                .setLimit(1)
                .build()

        Fitness.getHistoryClient(activity!!, GoogleSignIn.getAccountForExtension(activity!!, fitnessOptions))
                .readData(dataReadRequest)
                .addOnSuccessListener { dataReadResponse ->
//                    android.util.Log.e("System out", "dataReadResponse:${dataReadResponse.getDataSet(DataType.TYPE_WEIGHT).dataPoints[0].getValue(Field.FIELD_WEIGHT)}")
//                    val weight = if (dataReadRequest == null) 0 else dataReadResponse.getDataSet(DataType.TYPE_WEIGHT).dataPoints[0].getValue(Field.FIELD_WEIGHT)
//                    txtWeight.text = "Current Weight:- ${weight}"
                }
                .addOnFailureListener { e: Exception ->
                    android.util.Log.e("System out", "FAIL")
                }

        Fitness.getHistoryClient(activity!!, GoogleSignIn.getAccountForExtension(activity!!, fitnessOptions))
                .readDailyTotal(DataType.TYPE_CALORIES_EXPENDED)
                .addOnSuccessListener { result: DataSet ->
//                    val totalSteps = if (result.isEmpty) 0 else result.dataPoints[0].getValue(Field.FIELD_WEIGHT)
                    android.util.Log.e("System out", "total Step:$result")
                    val cal: Float = if (result.isEmpty) 0f else result.dataPoints[0].getValue(Field.FIELD_CALORIES).asFloat()
                    txtCal.text = "Cal:- ${cal.roundToInt()}"
                }
                .addOnFailureListener { e: Exception ->
                    android.util.Log.e("FragmentActivity.TAG", "There was a problem getting steps: " +
                            e.localizedMessage)
                }

    }

    private fun readPast() {
        val cal = Calendar.getInstance()
        val calstart = Calendar.getInstance()
//        calstart.add(Calendar.WEEK_OF_MONTH,-1)
//        val now = Date()
//        cal.time = now
        cal.add(Calendar.MONTH, -1)
        val startTime = calstart.timeInMillis
        val endTime = cal.timeInMillis

        val dateFormat: DateFormat = DateFormat.getDateInstance()
        android.util.Log.e(TAG, "Range Start: " + dateFormat.format(startTime))
        android.util.Log.e(TAG, "Range End: " + dateFormat.format(endTime))
        val dataReadRequest = DataReadRequest.Builder()
                .read(DataType.TYPE_WEIGHT)
                .setTimeRange(endTime, startTime, TimeUnit.MILLISECONDS)
                .setLimit(30)
                .build()

        Fitness.getHistoryClient(activity!!, GoogleSignIn.getAccountForExtension(activity!!, fitnessOptions))
                .readData(dataReadRequest)
                .addOnSuccessListener { dataReadResponse ->
                    android.util.Log.e("System out", "dataReadResponse:${dataReadResponse.getDataSet(DataType.TYPE_WEIGHT).dataPoints[0].getValue(Field.FIELD_WEIGHT)}")
                    val weight = dataReadResponse.getDataSet(DataType.TYPE_WEIGHT).dataPoints[0].getValue(Field.FIELD_WEIGHT)
//                    txtWeight.text = "Weight:- ${weight}"
                    txtWeight.append(":::Past Weight:- ${weight}")
                }
                .addOnFailureListener { e: Exception ->
                    android.util.Log.e("System out", "FAIL")
                }

    }

    private fun readPastCal() {
        val cal = Calendar.getInstance()//
        val calstart = Calendar.getInstance()
//        calstart.add(Calendar.WEEK_OF_MONTH,-1)
//        val now = Date()
//        cal.time = now
        cal.add(Calendar.MONTH, -1)
        val startTime = calstart.timeInMillis
        val endTime = cal.timeInMillis

        val dataReadRequest = DataReadRequest.Builder()
                .read(DataType.TYPE_CALORIES_EXPENDED)
                .setTimeRange(endTime, startTime, TimeUnit.MILLISECONDS)
                .setLimit(30)
                .build()

        Fitness.getHistoryClient(activity!!, GoogleSignIn.getAccountForExtension(activity!!, fitnessOptions))
                .readData(dataReadRequest)
                .addOnSuccessListener { dataReadResponse ->
                    android.util.Log.e("System out", "dataReadResponse:${dataReadResponse.getDataSet(DataType.TYPE_CALORIES_EXPENDED).dataPoints[0].getValue(Field.FIELD_CALORIES)}")
                    val cal: Float = dataReadResponse.getDataSet(DataType.TYPE_CALORIES_EXPENDED).dataPoints[0].getValue(Field.FIELD_CALORIES).asFloat()
//                    txtWeight.text = "Weight:- ${weight}"
                    txtCal.append(":::Past Cal:- ${cal.roundToInt()}")
                }
                .addOnFailureListener { e: Exception ->
                    android.util.Log.e("FragmentActivity.TAG", "There was a problem getting steps: " +
                            e.localizedMessage)
                }
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FitDataFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}