package com.q.webview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView

class ListAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_view_food, parent, false)
        return SubCategoryViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    inner class SubCategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView
        var tvPrice: TextView

        init {
            tvName = itemView.findViewById(R.id.name)
            tvPrice = itemView.findViewById(R.id.price)
        }
    }

    override fun getItemCount(): Int {
        return 20
    }
}