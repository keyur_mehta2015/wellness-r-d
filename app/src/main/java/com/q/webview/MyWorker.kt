package com.q.webview

import android.app.*
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataSet
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field

class MyWorker(private val context: Context, private val workerParams: WorkerParameters) :
        Worker(context, workerParams) {

    var fileReadWrite: InternalFileReadWrite? = null
    var totalSteps: Int = 0

    override fun doWork(): Result {
        fileReadWrite = InternalFileReadWrite(context)
        val getData = workerParams.inputData
        val channelId = getData.getString("channel_id")
        val title = getData.getString("title")
        val message = getData.getString("message")
        context.sendNotification(channelId, title, message)
        return Result.success()
    }

    private fun Context.sendNotification(
            channelId: String?,
            contentTitle: String?,
            contentText: String?
    ) {
        try {
            // Get an instance of the Notification manager
            readHistoryData()

            val mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // Android O requires a Notification Channel.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // Create the channel for the notification
                val mChannel =
                        NotificationChannel(channelId, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH)

                // Set the Notification Channel for the Notification Manager.
                mNotificationManager.createNotificationChannel(mChannel)
            }

            // Get a notification builder that's compatible with platform versions >= 4
            val builder = NotificationCompat.Builder(this)

            // Define the notification settings.
            builder.setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(contentTitle)
                    .setContentText(contentText)


            // Set the Channel ID for Android O.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                channelId?.let { builder.setChannelId(it) } // Channel ID
            } else {
                builder.priority = Notification.PRIORITY_HIGH
            }

            // Dismiss notification once the user touches it.
            builder.setAutoCancel(true)

            // Issue the notification
            mNotificationManager.notify(0, builder.build())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val fitnessOptions: FitnessOptions by lazy {
        FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
                .build()
    }

    private fun readHistoryData() {
        Fitness.getHistoryClient(context, GoogleSignIn.getAccountForExtension(context, fitnessOptions))
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener { result: DataSet ->
                    totalSteps = if (result.isEmpty) 0 else result.dataPoints[0].getValue(Field.FIELD_STEPS).asInt()
                    Log.e("System out", "inside service... total Step:$totalSteps");
                    fileReadWrite!!.writeFile(totalSteps)
                }
                .addOnFailureListener { e: Exception ->
                    Log.i("FragmentActivity.TAG", "There was a problem getting steps: " +
                            e.localizedMessage)
                }

    }
}