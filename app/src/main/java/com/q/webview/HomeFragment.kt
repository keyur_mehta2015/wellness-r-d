package com.q.webview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_welcome.*


class HomeFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val foodAdapter = ListAdapter()
        recyclerView.adapter = foodAdapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0 || dy < 0 && con.isShown) con.visibility = View.GONE

                if (dy > 0) {
                    // Scroll Down
                    if (con.isShown()) {
                        con.visibility = View.GONE;
                    }
                } else if (dy < 0) {
                    // Scroll Up
                    if (!con.isShown()) {
                        val animation = AnimationUtils.loadAnimation(activity, android.R.anim.fade_in)
                        con.startAnimation(animation)
                        con.visibility = View.VISIBLE
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                /*if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val animation = AnimationUtils.loadAnimation(activity, android.R.anim.fade_in)
                    con.startAnimation(animation)
                    con.visibility = View.VISIBLE
                }*/
                super.onScrollStateChanged(recyclerView, newState)
            }
        })

        /**
         * This also hide bottom con
         */
        /*recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 || dy < 0 && con.isShown) con.visibility = View.GONE
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val animation = AnimationUtils.loadAnimation(activity, android.R.anim.fade_in)
                    con.startAnimation(animation)
                    con.visibility = View.VISIBLE
                }
                super.onScrollStateChanged(recyclerView, newState)
            }
        })*/
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

}