package com.q.webview

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_splash.*

class SplashFragment : Fragment() {

    private var isFirstAnimation = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        /*Simple hold animation to hold ImageView in the centre of the screen at a slightly larger
        scale than the ImageView's original one.*/
        val hold = AnimationUtils.loadAnimation(activity, R.anim.hold)

        /* Translates ImageView from current position to its original position, scales it from
        larger scale to its original scale.*/


        /* Translates ImageView from current position to its original position, scales it from
        larger scale to its original scale.*/
        val hyperspaceJumpAnimation = AnimationUtils.loadAnimation(activity, R.anim.hyperspace_jump)

        val translateScale = AnimationUtils.loadAnimation(activity, R.anim.translate_scale)

//        val imageView: ImageView = findViewById(R.id.header_icon)

        header_icon.startAnimation(hyperspaceJumpAnimation)

        translateScale.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                if (!isFirstAnimation) {
                    header_icon.clearAnimation()
                    findNavController().navigate(R.id.actionMoveToWelcome, null)
                }
                isFirstAnimation = true
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })

        hold.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                header_icon.clearAnimation()
                header_icon.startAnimation(translateScale)
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })

        val handler = Handler()
        handler.postDelayed({ header_icon.startAnimation(hold) }, 2000)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                SplashFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}