package com.q.webview.services

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.os.SystemClock
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataSet
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.q.webview.InternalFileReadWrite
import com.q.webview.R
import com.q.webview.RestartService

class AlarmService : Service() {

    private var context: Context? = null
    var totalSteps: Int = 0
    private var counter = 0
    var fileReadWrite: InternalFileReadWrite? = null

    override fun onTaskRemoved(rootIntent: Intent?) {
        val intent = Intent(applicationContext, AlarmService::class.java)
        val pendingIntent = PendingIntent.getService(this, 1005, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager[AlarmManager.RTC, SystemClock.elapsedRealtime() + 5000] = pendingIntent
        Log.e("TAG", " service is call onTaskRemoved  ")
        super.onTaskRemoved(rootIntent)
    }

    override fun onCreate() {
        super.onCreate()
        Log.e("TAG", " service is created ")
        context = applicationContext
        fileReadWrite = InternalFileReadWrite(context)
        readHistoryData()
    }

    @SuppressLint("NewApi")
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        Log.e("TAG", " service onStartCommand is called $totalSteps")
        val requestCode = intent.getIntExtra("REQUEST_CODE", 0)
        val channelId = "default"
        val title = "Chord Health"
        val pendingIntent = PendingIntent.getActivity(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // Foreground Notification Context
        val notificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val notification = notificationBuilder
                .setContentTitle(title)
                .setSmallIcon(R.drawable.ic_stat_icon_128x128)
//                .setContentText("Today's Steps==>$totalSteps " + "  " + DateFormat.getDateTimeInstance().format(Date()))
                .setContentText("Today's Steps Demo: $totalSteps")
                .setAutoCancel(true)
                .setColor(context!!.resources.getColor(R.color.notification))
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .build()

        // Notification　Channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, title, NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = "Silent Notification"
            channel.setSound(null, null)
            channel.enableLights(false)
            channel.lightColor = Color.BLUE
            channel.enableVibration(false)
            notificationManager.createNotificationChannel(channel)
        }
        startForeground(1, notification)

//        startForegroundService(intent)

        // set Alarm
        setNextAlarmService(context!!)

        //return START_NOT_STICKY;
        return START_STICKY
        //return START_REDELIVER_INTENT;
    }

    @SuppressLint("NewApi")
    private fun setNextAlarmService(context: Context) {

        // 15 min
        val repeatPeriod = 10 * 60 * 1000.toLong()
        val intent = Intent(context, AlarmService::class.java)
        val startMillis = System.currentTimeMillis() + repeatPeriod
        val pendingIntent = PendingIntent.getService(context, 0, intent, 0)
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        counter = counter + 1
        if (Build.VERSION.SDK_INT >= 23) {
            Log.e("TAG", "alarm set version O ")
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, startMillis, pendingIntent)
        } else if (Build.VERSION.SDK_INT >= 19) {
            Log.e("TAG", "alarm set version setExact method ")
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, startMillis, pendingIntent)
        } else {
            Log.e("TAG", "alarm set version set method ")
            alarmManager[AlarmManager.RTC_WAKEUP, startMillis] = pendingIntent
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        val restartService = RestartService()
        val intentFilter = IntentFilter("in.alarm.RestartSensor")
        context!!.registerReceiver(restartService, intentFilter)
        Log.e("TAG", " service is destroy")
    }

    fun stopServiceWithAlarm() {
        stopAlarmService()
        stopSelf()
    }

    private fun stopAlarmService() {
        val indent = Intent(context, AlarmService::class.java)
        val pendingIntent = PendingIntent.getService(context, 0, indent, 0)
        val alarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager?.cancel(pendingIntent)
    }

    private val fitnessOptions: FitnessOptions by lazy {
        FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
                .build()
    }

    private fun readHistoryData() {
        Fitness.getHistoryClient(this, GoogleSignIn.getAccountForExtension(this, fitnessOptions))
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener { result: DataSet ->
                    totalSteps = if (result.isEmpty) 0 else result.dataPoints[0].getValue(Field.FIELD_STEPS).asInt()
                    Log.e("System out", "inside service... total Step:$totalSteps");
                    fileReadWrite!!.writeFile(totalSteps)

                }
                .addOnFailureListener { e: Exception ->
                    Log.i("FragmentActivity.TAG", "There was a problem getting steps: " +
                            e.localizedMessage)
                }

    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

}